function love.load()
	math.randomseed(os.time())
	
	buttonx = {}	  -- x
		buttonx[1] = {280}
		buttonx[2] = {313}
		buttonx[3] = {346}		
		buttonx[4] = {277}
		buttonx[5] = {311}
		buttonx[6] = {346}
		buttonx[7] = {275}
		buttonx[8] = {309}
		buttonx[9] = {344}
		buttonx[10] = {309}
		
	buttony = {}	  -- y
		buttony[1] = {293}
		buttony[2] = {294}
		buttony[3] = {295}		
		buttony[4] = {327}
		buttony[5] = {328}
		buttony[6] = {329}
		buttony[7] = {361}
		buttony[8] = {361}
		buttony[9] = {363}
		buttony[10] = {395}
	
	code = {}
		code[1] = math.random(0,9)
		code[2] = math.random(0,9)
		code[3] = math.random(0,9)
		code[4] = math.random(0,9)
		
	
	background = love.graphics.newImage("bomb.png")
	hand = love.graphics.newImage("hand.png")
	tick = love.graphics.newImage("light.png")
	bombtimer = 0
	c4timer = 35
	displaytick = false
    love.mouse.setVisible(true)
	coc = 0
end

function love.update(dt)
	gdt = dt							-- global dt
	mx, my = love.mouse.getPosition()
	bombtimer = (bombtimer + dt)		-- Every second
	if bombtimer >= 1 then
		c4timer = (c4timer - 1)
		bombtimer = (bombtimer - 1)		-- Reset timer
	end
	coc = coc + dt		-- debug lol
	if coc > 1 then print("code: "..code[1]..code[2]..code[3]..code[4]) coc = -300 end
end

function love.mousepressed(x, y, button)
	if button == 'l' then
		for i,v in pairs(buttonx, buttony) do
			for fooby = 1, 10 do
				if mx > buttonx[fooby][1] and mx < buttonx[fooby][1] + 25 and my > buttony[fooby][1] and my < buttony[fooby][1] + 25 then
					for zeeky = 1, 4 do
						if i == code[zeeky] then print(i) end
					end
					return
				end
			end
		end
	end
end

function love.mousereleased(button)
end

function love.draw()
	love.graphics.draw(background, 0, 0)
	love.graphics.draw(hand, (mx - 10), (my - 6))
	love.graphics.print(mx, 250, 0)
	love.graphics.print(my, 250, 15)
	love.graphics.print("timer: "..bombtimer)
	love.graphics.print("c4timer: "..c4timer, 0, 15)
end